package com.example.demo.service;

public interface INotifyService {
    void sendNotification(String message);
}
