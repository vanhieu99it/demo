package com.example.demo.service.impl;

import com.example.demo.service.INotifyService;
import com.example.demo.config.NotificationClientProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@RequiredArgsConstructor
@Service
public class NotifyServiceImpl implements INotifyService {

    private final NotificationClientProperties notificationClientProperties;

    @Override
    public void sendNotification(String message) {
        RestTemplate restTemplate = new RestTemplate();
        String url = this.notificationClientProperties.getUrl();
        String token = this.notificationClientProperties.getToken(); //iHqWJQ3tvtqmALk5UJlEE4hToXjn0PM7ysghfgYAb8p

        HttpHeaders header = new HttpHeaders();
        header.setBearerAuth(token);
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        header.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("message", message);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, header);
        restTemplate.postForObject(url, request, String.class);
    }
}
