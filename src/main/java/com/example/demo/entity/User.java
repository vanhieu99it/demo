package com.example.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {
    private Integer user_id;
    private String username;
    private String password;
    private String email;
    private String full_name;
    private String role;
    private LocalDateTime created_at;
}
