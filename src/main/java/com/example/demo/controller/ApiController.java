package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.request.notifyRequest;
import com.example.demo.service.INotifyService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ApiController {
    @Autowired
    private INotifyService notifyService;

    @Autowired
    private UserService userService;

    @GetMapping("/getallusers")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/notify")
    public void notifyMessage(@RequestBody notifyRequest request) throws Exception{
        this.notifyService.sendNotification(request.getMessage());
    }

    @GetMapping("/hooks")
    public String hook() {
        return "Hello, test webhook";
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello !!!";
    }


}
