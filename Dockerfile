# Sử dụng hình ảnh chính thức của OpenJDK làm hình ảnh nền tảng
FROM openjdk:11-jdk-slim

LABEL authors="Hieu"

# Thêm JAR vào hình ảnh
COPY target/demo-0.0.1-SNAPSHOT.jar demo.jar

# Chạy ứng dụng
ENTRYPOINT ["java", "-jar", "/demo.jar"]